# Directv Microservices with clean architecture POC

---

# Directv POC backend

## Installing development environment

You will need to install the following tools: 

1. NET SDK 5 & runtime from [Microsoft Site](https://dotnet.microsoft.com/en-us/download/visual-studio-sdks).
2. Make sure you have [Docker](https://docs.docker.com/get-docker/) installed.


## Environment configurations

Next, in order to run and debug the application, you need to:

1. To debug the application you will need to set the following environments variables used to authenticate Forgerock API requests.  
   - **ForgeRockApi_Client**  
   - **ForgeRockApi_Secret**  
2. Before open project's solution remember to run Visual Studio as administrator, you will need it to read environment variables.


## Start up the application with Docker's containers

1. Locate the docker-compose files in a powershell command prompt or another of your choice. You may do it from Visual Studio or through file explorer.  
2. Next, run docker-compose command to deploy the app in containers:  
```
	docker-compose -f docker-compose.yml -f docker-compose.override.yml up -d
```  
   - Flag -f specifies the file/s what you want to run.  
   - Flag -d run the command in detach mode.  

3. After the docker-compose is done, you may browse the application:  
   - Forgerock API swagger [http://localhost:8000/swagger](http://localhost:8000/swagger)  
   - Ocelot Gateway API [http://localhost:8010/](http://localhost:8010/)  
   - Portainer dashboard [http://localhost:9000/](http://localhost:9000/)  
   - Web App VueJS [http://localhost:8050/Login](http://localhost:8050/Login)  

4. Finally to shutdown the application containers, in the same prompt, you may run the command:  
```
	docker-compose -f docker-compose.yml -f docker-compose.override.yml down
```

5. In addition, if you need to regenerate application's docker images, you may run this command to delete current images:
```
	docker rm $(docker ps -aq)
```  
   Then you may repeat step #4.

---

# Directv POC frontend

## Installing development environment

1. Install [NodeJS](https://nodejs.org/es/) lastest version

## Setting web app environment and run the Single Page Application 

1. Open a terminal inside the project folder **~ROOT\src\MiDirectv.Poc.WebApps\Login.VueJs\directv-login**  
2. Run next command to get all the necesary node modules for the front end to work  
```
	npm install
```  
3. Run next command to start frontend application  
```
    npm run serve
```

---

# Additional resources

- In folder **~ROOT\public** you will find a Postman collection with requests to POC's API projects and containers.