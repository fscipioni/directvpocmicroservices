export const settings = {
    ENV: "development",
    VUE_APP_ROOT_API : "http://localhost:8010",
    VUE_APP_LOGIN_RESOURCE : "/api/UserAccount"
}
