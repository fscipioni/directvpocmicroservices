import Vue from 'vue'
import axios from 'axios'

export const actions = {
    async submitLogin({ commit }, form) {
      await commit('checkFormStatusStart');
      
      console.log(`${Vue.prototype.$baseApi}${Vue.prototype.$loginResource}`);

      const values = {
        "grantType": "",
        "username": form.email,
        "password": form.password,
        "scope": ""
      }
        try {
            axios.defaults.headers.common['Access-Control-Allow-Origin'] = window.location.origin;

            let response = await axios.post(`${Vue.prototype.$baseApi}${Vue.prototype.$loginResource}`, values, {
                headers: { 
                    "Content-Type": "application/json"
                }
            })
            
            const params = {
              message : 'Login Correcto', 
              status : response.status,
              formulario : form
            };

            if (response.status == '200') {
              await commit('checkFormStatusSuccess', params);

              return true;
            }

            return false;
        }
      catch (e) {
        const params = {
            message : 'Login Incorrecto',
            status : '500',
        };
        await commit('checkFormStatusFailure', params);
        return false
      }
    }
}