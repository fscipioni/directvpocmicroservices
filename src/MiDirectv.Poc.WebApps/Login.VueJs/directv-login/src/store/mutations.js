export const mutations = {
  checkFormStatusStart(state) {
    let list = state.form;
      list = {
        email: '',
        password: ''
      }
    state.form = {
        ...list,
    }
  },
  checkFormStatusSuccess(state, { message, status, formulario }) {
    let list = state.form;
        list = {
            email: formulario.email,
            password: formulario.password,
            message: message,
            status: status
        }
    state.form = {
        ...list,
    }
  },
  checkFormStatusFailure(state, { message, status }) {
    let list = state.form;
      list = 
        {
            message: message,
            status: status
        },
    state.form = {
        ...list,
    }
  },
}