﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MiDirectv.Poc.ForgeRock.API.Controllers;
using MiDirectv.Poc.ForgeRock.Application.Exceptions;
using MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.CreateUserAccount;
using MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.DeleteUserAccount;
using MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Queries.LoginUserAccountQuery;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MiDirectv.Poc.ForgeRock.API.Test.Controllers
{
    public class UserAccountControllerTest
    {
        [Fact]
        public async Task LoginUserAccountSuccessfully()
        {
            // Arrange
            Mock<IMediator> loginUserMediatorMock = new Mock<IMediator>();

            LoginUserAccountViewModel model = new LoginUserAccountViewModel()
            {
                AccessToken = Guid.NewGuid().ToString(),
                RefreshToken = Guid.NewGuid().ToString(),
                Expiration = new Random().Next(1, 1000000),
                Scope = "bearer",
                TokenType = "profile"
            };

            loginUserMediatorMock.Setup(mediator => mediator.Send(It.IsAny<LoginUserAccountQuery>(), It.IsAny<CancellationToken>()))
                                 .ReturnsAsync(model);

            UserAccountController controller = new UserAccountController(loginUserMediatorMock.Object);

            LoginUserAccountQuery query = new LoginUserAccountQuery()
            {
                Username = "poc.microservices@yopmail.com",
                Password = "Directv.00"
            };

            // Act
            ActionResult<LoginUserAccountViewModel> response = await controller.LoginUser(query);

            // Assert
            Assert.IsType<OkObjectResult>(response.Result);
        }

        [Fact]
        public async Task CreateUserAccountSuccessfully()
        {
            // Arrange
            Mock<IMediator> createUserMediatorMock = new Mock<IMediator>();

            string guid = Guid.NewGuid().ToString();

            createUserMediatorMock.Setup(mediator => mediator.Send(It.IsAny<CreateUserAccountCommand>(), It.IsAny<CancellationToken>()))
                                  .ReturnsAsync(guid);

            UserAccountController controller = new UserAccountController(createUserMediatorMock.Object);

            CreateUserAccountCommand command = new CreateUserAccountCommand()
            {
                Country = "AR",
                AccountNumber = "12345678",
                Email = "midirectv12345678@yopmail.com",
                UserName = "midirectv12345678@yopmail.com",
                Name = "midirectv12345678",
                ShortUserName = "midirectv12345678",
                Password = "Directv.00",
                UserType = "OTT-LATAM",
                Tel = "111122223333"
            };

            // Act
            ActionResult<string> response = await controller.CreateUser(command);

            // Assert
            Assert.IsType<AcceptedResult>(response.Result);
        }

        [Fact]
        public async Task DeleteUserAccountSuccessfully()
        {
            // Arrange
            Mock<IMediator> deleteUserMediatorMock = new Mock<IMediator>();

            deleteUserMediatorMock.Setup(mediator => mediator.Send(It.IsAny<DeleteUserAccountCommand>(), It.IsAny<CancellationToken>()))
                                  .ReturnsAsync(Unit.Value);

            UserAccountController controller = new UserAccountController(deleteUserMediatorMock.Object);

            DeleteUserAccountCommand command = new DeleteUserAccountCommand()
            {
                Country = "AR",
                UserName = "midirectv12345678@yopmail.com",
                UserType = "OTT-LATAM"
            };

            // Act
            ActionResult response = await controller.DeleteUser(command);

            // Assert
            Assert.IsType<NoContentResult>(response);
        }
    }
}