﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace MiDirectv.Poc.ForgeRock.Infrastructure.Extensions
{
    public static class HttpClientExtensions
    {
        public static HttpClient ConfigureServiceClient(this HttpClient client)
        {
            string credentialString = $"{ Environment.GetEnvironmentVariable("ForgeRockApi_Client") }:{ Environment.GetEnvironmentVariable("ForgeRockApi_Secret") }";

            string base64EncodedAuthenticationString = Convert.ToBase64String(Encoding.ASCII.GetBytes(credentialString));

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

            return client;
        }

        public static async Task<T> ReadContentAs<T>(this HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode) throw new ApplicationException($"Something went wrong calling the API: { response.ReasonPhrase }.");

            string dataAsString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return JsonConvert.DeserializeObject<T>(dataAsString);
        }
    }
}