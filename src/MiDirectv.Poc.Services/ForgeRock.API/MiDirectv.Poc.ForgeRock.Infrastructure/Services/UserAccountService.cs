﻿using MiDirectv.Poc.ForgeRock.Application.Contracts;
using MiDirectv.Poc.ForgeRock.Application.Models;
using MiDirectv.Poc.ForgeRock.Domain.Entities;
using MiDirectv.Poc.ForgeRock.Infrastructure.Extensions;
using Newtonsoft.Json;

namespace MiDirectv.Poc.ForgeRock.Infrastructure.Services
{
    public class UserAccountService : IUserAccountService
    {
        private readonly HttpClient _client;
        private readonly ApiConfiguration _apiConfiguration;

        public UserAccountService(HttpClient client, ApiConfiguration apiConfiguration)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _apiConfiguration = apiConfiguration ?? throw new ArgumentNullException(nameof(apiConfiguration));

            _client.BaseAddress = new Uri(apiConfiguration.BaseAddress);
            _client.ConfigureServiceClient();
        }

        public async Task<UserAccountGranted> Login(UserAccountCredential credentials)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, new Uri($"{ _apiConfiguration.BaseAddress }{ _apiConfiguration.Resource.LoginUser }"))
            {
                Content = new FormUrlEncodedContent(new Dictionary<string, string>()
                {
                    { "grant_type", credentials.GrantType },
                    { "username", credentials.Username },
                    { "password", credentials.Password },
                    { "scope", credentials.Scope }
                })
            };

            HttpResponseMessage response = await _client.SendAsync(request);

            if (!response.IsSuccessStatusCode) throw new ApplicationException($"Something went wrong trying to login user account. Please check if the data is valid. Response error: { response.Content.ReadAsStringAsync().Result }");

            return await response.ReadContentAs<UserAccountGranted>();
        }

        public async Task<string> Create(CreateUserAccount userAccount)
        {
            HttpResponseMessage response = await _client.PostAsync(_apiConfiguration.Resource.CreateUser, CreateStringContent(userAccount));

            CreatedUserAccount createdAccount = await response.ReadContentAs<CreatedUserAccount>();

            return createdAccount?.Account?.AccountId;
        }

        public async Task Delete(UserAccount userAccount)
        {
            HttpResponseMessage response = await _client.PostAsync(_apiConfiguration.Resource.DeleteUser, CreateStringContent(userAccount));

            if (!response.IsSuccessStatusCode) throw new ApplicationException("Something went wrong trying to delete user account.");
        }

        private HttpContent CreateStringContent<T>(T objectToSerialize)
        {
            return new StringContent(JsonConvert.SerializeObject(objectToSerialize));
        }
    }
}