﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MiDirectv.Poc.ForgeRock.Application.Contracts;
using MiDirectv.Poc.ForgeRock.Application.Models;
using MiDirectv.Poc.ForgeRock.Infrastructure.Services;

namespace MiDirectv.Poc.ForgeRock.Infrastructure
{
    public static class DependencyInjectionRegistration
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            //services.Configure<ApiConfiguration>(options => configuration.GetSection("ApiConfiguration"));

            services.AddTransient<IUserAccountService, UserAccountService>();

            return services;
        }
    }
}