﻿using Newtonsoft.Json;

namespace MiDirectv.Poc.ForgeRock.Domain.Entities
{
    public class UserAccount
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("businessUnitType")]
        public string UserType { get; set; }

        [JsonProperty("iso2Code")]
        public string Country { get; set; }
    }
}