﻿using Newtonsoft.Json;

namespace MiDirectv.Poc.ForgeRock.Domain.Entities
{
    public class CreateUserAccount : UserAccount
    {
        [JsonProperty("mail")]
        public string Email { get; set; }

        [JsonProperty("givenName")]
        public string Name { get; set; }

        [JsonProperty("accountID")]
        public string AccountNumber { get; set; }

        [JsonProperty("sn")]
        public string ShortUserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("telephoneNumber")]
        public string Tel { get; set; }
    }
}