﻿using Newtonsoft.Json;

namespace MiDirectv.Poc.ForgeRock.Domain.Entities
{
    public class Account
    {
        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("vrioCustomerId")]
        public string VrioCUstomerId { get; set; }

        [JsonProperty("iso2Code")]
        public string Country { get; set; }

        [JsonProperty("businessUnitType")]
        public string AccountType { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("timeCreated")]
        public DateTime CreationTime { get; set; }

        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("_rev")]
        public string Rev { get; set; }

        [JsonProperty("party")]
        public object Party { get; set; }
    }
}
