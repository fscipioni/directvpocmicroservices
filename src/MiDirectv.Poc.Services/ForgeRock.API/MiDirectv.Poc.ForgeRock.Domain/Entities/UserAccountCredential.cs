﻿using Newtonsoft.Json;

namespace MiDirectv.Poc.ForgeRock.Domain.Entities
{
    public class UserAccountCredential
    {
        private string _grantType;

        public string GrantType { 
            get
            {
                if (string.IsNullOrEmpty(_grantType)) return "password";

                return _grantType;
            }
            set
            {
                _grantType = value;
            }
        }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Scope { get; set; }
    }
}