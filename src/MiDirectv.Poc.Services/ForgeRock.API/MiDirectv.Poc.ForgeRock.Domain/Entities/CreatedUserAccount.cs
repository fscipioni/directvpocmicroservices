﻿using Newtonsoft.Json;

namespace MiDirectv.Poc.ForgeRock.Domain.Entities
{
    public class CreatedUserAccount
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        public User User { get; set; }
        public Account Account { get; set; }
    }
}
