﻿using Newtonsoft.Json;
using System;

namespace MiDirectv.Poc.ForgeRock.Domain.Entities
{
    public class User : UserAccount
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("mail")]
        public string Email { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("givenName")]
        public string GivenName { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("sn")]
        public string ShortUserName { get; set; }

        [JsonProperty("accountStatus")]
        public string UserStatus { get; set; }

        [JsonProperty("timeCreated")]
        public DateTime CreationTime { get; set; }

        [JsonProperty("partyId")]
        public object PartyId { get; set; }

        [JsonProperty("familyName")]
        public string FamilyName { get; set; }

        [JsonProperty("effectiveRoles")]
        public object EffectiveRoles { get; set; }

        [JsonProperty("effectiveAssignments")]
        public object EffectiveAssignments { get; set; }

        [JsonProperty("_rev")]
        public string Rev { get; set; }

        [JsonProperty("telephoneNumber")]
        public string Tel { get; set; }
    }
}