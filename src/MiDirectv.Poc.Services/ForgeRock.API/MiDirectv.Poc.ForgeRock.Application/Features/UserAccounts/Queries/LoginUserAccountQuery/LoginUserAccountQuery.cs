﻿using MediatR;
using System;

namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Queries.LoginUserAccountQuery
{
    public class LoginUserAccountQuery : IRequest<LoginUserAccountViewModel>
    {
        public string GrantType { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Scope { get; set; }
    }
}
