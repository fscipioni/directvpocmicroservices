﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using MiDirectv.Poc.ForgeRock.Application.Contracts;
using MiDirectv.Poc.ForgeRock.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Queries.LoginUserAccountQuery
{
    public class LoginUserAccountQueryHandler : IRequestHandler<LoginUserAccountQuery, LoginUserAccountViewModel>
    {
        private readonly IUserAccountService _userAccountService;
        private readonly IMapper _mapper;
        private readonly ILogger<LoginUserAccountQueryHandler> _logger;

        public LoginUserAccountQueryHandler(IUserAccountService userAccountService, IMapper mapper, ILogger<LoginUserAccountQueryHandler> logger)
        {
            _userAccountService = userAccountService ?? throw new ArgumentNullException(nameof(userAccountService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<LoginUserAccountViewModel> Handle(LoginUserAccountQuery request, CancellationToken cancellationToken)
        {
            UserAccountCredential credentials = _mapper.Map<UserAccountCredential>(request);

            UserAccountGranted userGranted = await _userAccountService.Login(credentials);

            _logger.LogInformation($"User granted. Expires in { userGranted.Expiration }");

            return _mapper.Map<LoginUserAccountViewModel>(userGranted);
        }
    }
}