﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Queries.LoginUserAccountQuery
{
    public class LoginUserAccountQueryValidator : AbstractValidator<LoginUserAccountQuery>
    {
        public LoginUserAccountQueryValidator()
        {
            RuleFor(query => query.Username).NotNull().WithMessage("Username is required.")
                                            .NotEmpty()
                                            .EmailAddress().WithMessage("Username is not valid.");

            RuleFor(query => query.Password).NotNull().WithMessage("Password is required.")
                                            .NotEmpty()
                                            .MinimumLength(6).WithMessage("Password must have at least 6 characters.");
        }
    }
}
