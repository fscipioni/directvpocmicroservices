﻿namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Queries.LoginUserAccountQuery
{
    public class LoginUserAccountViewModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string Scope { get; set; }
        public string TokenType { get; set; }
        public int Expiration { get; set; }
    }
}