﻿using MediatR;

namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.CreateUserAccount
{
    public class CreateUserAccountCommand : IRequest<string>
    {
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string ShortUserName { get; set; }
        public string Password { get; set; }
        public string Tel { get; set; }
    }
}
