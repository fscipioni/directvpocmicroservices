﻿using FluentValidation;

namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.CreateUserAccount
{
    public class CreateUserAccountCommandValidator : AbstractValidator<CreateUserAccountCommand>
    {
        public CreateUserAccountCommandValidator()
        {
            RuleFor(o => o.AccountNumber).NotEmpty().WithMessage("{AccountNumber} is required.")
                                         .NotNull();

            RuleFor(o => o.Country).NotEmpty().WithMessage("{Country} is required.")
                                   .NotNull()
                                   .MaximumLength(2).WithMessage("{Country} must not exceed 2 characters.");

            RuleFor(o => o.UserName).NotEmpty().WithMessage("{UserName} is required.")
                                    .NotNull();

            RuleFor(o => o.Email).NotEmpty().WithMessage("{Email} is required.")
                                 .NotNull()
                                 .EmailAddress().WithMessage("{Email} must be valid.");

            RuleFor(o => o.UserType).NotEmpty().WithMessage("{UserType} is required.")
                                    .NotNull();

            RuleFor(o => o.Password).NotEmpty().WithMessage("{Password} is required.")
                                    .NotNull()
                                    .MinimumLength(8).WithMessage("{Password} must have at least 8 characters.");
        }
    }
}
