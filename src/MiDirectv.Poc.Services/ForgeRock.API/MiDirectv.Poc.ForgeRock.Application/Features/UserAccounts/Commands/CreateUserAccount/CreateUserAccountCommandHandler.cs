﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using MiDirectv.Poc.ForgeRock.Application.Contracts;
using System;
using System.Threading;
using System.Threading.Tasks;
using UserAccount = MiDirectv.Poc.ForgeRock.Domain.Entities.CreateUserAccount;

namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.CreateUserAccount
{
    public class CreateUserAccountCommandHandler : IRequestHandler<CreateUserAccountCommand, string>
    {
        private readonly IUserAccountService _userAccountService;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateUserAccountCommandHandler> _logger;

        public CreateUserAccountCommandHandler(IUserAccountService userAccountService, IMapper mapper, ILogger<CreateUserAccountCommandHandler> logger)
        {
            _userAccountService = userAccountService ?? throw new ArgumentNullException(nameof(userAccountService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<string> Handle(CreateUserAccountCommand request, CancellationToken cancellationToken)
        {
            UserAccount createUserAccount = _mapper.Map<UserAccount>(request);

            string id = await _userAccountService.Create(createUserAccount);

            if (string.IsNullOrEmpty(id)) throw new ApplicationException("User account registration failed.");

            _logger.LogInformation($"User account { id } created.");

            return id;
        }
    }
}
