﻿using FluentValidation;

namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.DeleteUserAccount
{
    public class DeleteUserAccountCommandValidator : AbstractValidator<DeleteUserAccountCommand>
    {
        public DeleteUserAccountCommandValidator()
        {
            RuleFor(o => o.Country).NotEmpty().WithMessage("{Country} is required.")
                                   .NotNull()
                                   .MaximumLength(2).WithMessage("{Country} must not exceed 2 characters.");

            RuleFor(o => o.UserName).NotEmpty().WithMessage("{UserName} is required.")
                                    .NotNull();

            RuleFor(o => o.UserType).NotEmpty().WithMessage("{UserType} is required.")
                                    .NotNull();
        }
    }
}
