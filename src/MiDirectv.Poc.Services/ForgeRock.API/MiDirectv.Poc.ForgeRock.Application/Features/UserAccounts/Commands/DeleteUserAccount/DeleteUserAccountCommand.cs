﻿using MediatR;

namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.DeleteUserAccount
{
    public class DeleteUserAccountCommand : IRequest
    {
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string Country { get; set; }
    }
}
