﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using MiDirectv.Poc.ForgeRock.Application.Contracts;
using MiDirectv.Poc.ForgeRock.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.DeleteUserAccount
{
    public class DeleteUserAccountCommandHandler : IRequestHandler<DeleteUserAccountCommand>
    {
        private readonly IUserAccountService _userAccountService = null;
        private readonly IMapper _mapper = null;
        private readonly ILogger<DeleteUserAccountCommandHandler> _logger = null;

        public DeleteUserAccountCommandHandler(IUserAccountService userAccountService, IMapper mapper, ILogger<DeleteUserAccountCommandHandler> logger)
        {
            _userAccountService = userAccountService ?? throw new ArgumentNullException(nameof(userAccountService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(DeleteUserAccountCommand request, CancellationToken cancellationToken)
        {
            UserAccount userAccount = _mapper.Map<UserAccount>(request);

            await _userAccountService.Delete(userAccount);

            return Unit.Value;
        }
    }
}
