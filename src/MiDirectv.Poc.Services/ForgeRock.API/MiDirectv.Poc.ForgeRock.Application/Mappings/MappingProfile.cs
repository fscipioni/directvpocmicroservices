﻿using AutoMapper;
using MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.CreateUserAccount;
using MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.DeleteUserAccount;
using MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Queries.LoginUserAccountQuery;
using MiDirectv.Poc.ForgeRock.Domain.Entities;

namespace MiDirectv.Poc.ForgeRock.Application.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateUserAccountCommand, CreateUserAccount>().ReverseMap();
            CreateMap<DeleteUserAccountCommand, UserAccount>().ReverseMap();
            CreateMap<LoginUserAccountQuery, UserAccountCredential>().ReverseMap();
            CreateMap<LoginUserAccountViewModel, UserAccountGranted>().ReverseMap();
        }
    }
}
