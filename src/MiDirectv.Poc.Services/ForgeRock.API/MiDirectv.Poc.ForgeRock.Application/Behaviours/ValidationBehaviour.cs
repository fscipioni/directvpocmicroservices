﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace MiDirectv.Poc.ForgeRock.Application.Behaviours
{
    public class ValidationBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public ValidationBehaviour(IEnumerable<IValidator<TRequest>> validators)
        {
            _validators = validators ?? throw new ArgumentNullException(nameof(validators));
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            if (_validators.Any())
            {
                IValidationContext context = new ValidationContext<TRequest>(request);

                ValidationResult[] validationResults = await Task.WhenAll(_validators.Select(validator => validator.ValidateAsync(context, cancellationToken)));

                List<ValidationFailure> failures = validationResults.SelectMany(result => result.Errors).Where(failure => failure != null).ToList();

                if (failures.Any()) throw new ValidationException(failures);
            }

            return await next();
        }
    }
}
