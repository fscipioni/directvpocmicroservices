﻿using MiDirectv.Poc.ForgeRock.Domain.Entities;
using System.Threading.Tasks;

namespace MiDirectv.Poc.ForgeRock.Application.Contracts
{
    public interface IUserAccountService
    {
        Task<UserAccountGranted> Login(UserAccountCredential credentials);
        Task<string> Create(CreateUserAccount userAccount);
        Task Delete(UserAccount userAccount);
    }
}
