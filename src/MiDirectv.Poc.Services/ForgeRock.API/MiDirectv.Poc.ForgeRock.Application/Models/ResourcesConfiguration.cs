﻿namespace MiDirectv.Poc.ForgeRock.Application.Models
{
    public class ResourcesConfiguration
    {
        public string LoginUser { get; set; }
        public string CreateUser { get; set; }
        public string DeleteUser { get; set; }

        public ResourcesConfiguration()
        {

        }
    }
}
