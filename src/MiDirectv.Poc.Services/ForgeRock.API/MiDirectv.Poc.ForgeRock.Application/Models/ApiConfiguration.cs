﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiDirectv.Poc.ForgeRock.Application.Models
{
    public class ApiConfiguration
    {
        public string BaseAddress { get; set; }
        public ResourcesConfiguration Resource { get; set; }

        public ApiConfiguration()
        {

        }
    }
}
