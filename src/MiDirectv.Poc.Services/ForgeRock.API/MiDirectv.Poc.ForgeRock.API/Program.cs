using Microsoft.OpenApi.Models;
using MiDirectv.Poc.ForgeRock.API.Extensions;
using MiDirectv.Poc.ForgeRock.Application;
using MiDirectv.Poc.ForgeRock.Application.Models;
using MiDirectv.Poc.ForgeRock.Infrastructure;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "MiDirectv.Poc.ForgeRock.API", Version = "v1" });
});

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "AllowAll",
                      policy =>
                      {
                          policy.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                      });
});


builder.Services.AddHttpClient();

ApiConfiguration apiConfiguration = new();
builder.Configuration.GetSection("ApiConfiguration").Bind(apiConfiguration);
builder.Services.AddSingleton(apiConfiguration);

builder.Services.AddApplicationServices();
builder.Services.AddInfrastructureServices(builder.Configuration);

WebApplication app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.ConfigureExceptionHandler();

app.UseCors("AllowAll");

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();