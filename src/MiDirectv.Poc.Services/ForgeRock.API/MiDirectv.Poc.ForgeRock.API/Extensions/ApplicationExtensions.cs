﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using MiDirectv.Poc.ForgeRock.Application.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiDirectv.Poc.ForgeRock.API.Extensions
{
    public static class ApplicationExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(error =>
            {
                error.Run(async context =>
                {
                    IExceptionHandlerFeature errorContext = context.Features.Get<IExceptionHandlerFeature>();


                    if (errorContext != null)
                    {
                        bool isValidationException = errorContext.Error.GetType().Name.Equals("ValidationException");

                        string message = isValidationException ? errorContext.Error.Message : "Internal server error. Please try again later.";
                        context.Response.StatusCode = isValidationException ? StatusCodes.Status400BadRequest : StatusCodes.Status500InternalServerError;
                        context.Response.ContentType = "application/json";

                        await context.Response.WriteAsync(new 
                        {
                            StatusCode = context.Response.StatusCode,
                            Message = message
                        }.ToString());
                    }
                });
            });
        }
    }
}