﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.CreateUserAccount;
using MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Commands.DeleteUserAccount;
using MiDirectv.Poc.ForgeRock.Application.Features.UserAccounts.Queries.LoginUserAccountQuery;
using System;
using System.Threading.Tasks;

namespace MiDirectv.Poc.ForgeRock.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UserAccountController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UserAccountController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpPost("[action]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<LoginUserAccountViewModel>> LoginUser([FromBody] LoginUserAccountQuery query)
        {
            LoginUserAccountViewModel userGranted = await _mediator.Send(query);

            return Ok(userGranted);
        }

        [HttpPost("[action]")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> CreateUser([FromBody] CreateUserAccountCommand command)
        {
            string id = await _mediator.Send(command);

            return Accepted(id);
        }

        [HttpPost("[action]")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteUser([FromBody] DeleteUserAccountCommand command)
        {
            await _mediator.Send(command);

            return NoContent();
        }
    }
}